package com.adventofcode.season2015

import java.io.{BufferedReader, InputStreamReader}
import java.nio.charset.StandardCharsets
import scala.collection.JavaConverters._

object Day01Part01 {
  def main(args: Array[String]): Unit = {
    val reader = new BufferedReader(
      new InputStreamReader(System.in, StandardCharsets.UTF_8))

    val floor: Int = reader.lines()
      .iterator.asScala.toStream
      .flatMap(decodeInstruction).sum

    println(floor)
  }

  def decodeInstruction(instructions: String): Stream[Int] = {
    instructions.split("").toStream.map({
      case "(" => 1
      case ")" => -1
      case _ => 0
    })
  }
}
