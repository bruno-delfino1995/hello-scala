name := "advent-of-code"

version := "0.1"

scalaVersion := "2.12.8"

val challengeClass = "com.adventofcode.season2015.Day01Part01"

mainClass in (Compile, run) := Some(challengeClass)
mainClass in (Compile, packageBin) := Some(challengeClass)
