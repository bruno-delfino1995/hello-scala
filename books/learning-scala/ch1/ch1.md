# Getting started with the Scalable Language

- Scala REPL's History is saved across sessions
- When a command returns something, it's stored under a variable starting with `res`
  - This can be seen right below the command, the "output" of it will be a value declaration

## Exercises

1. As Scala is built on top of JVM, we can use the output buffer stored statically under `System`. I think the REPL supports all the data-types available in Scala and prints those which are printable
2. `scala> 22.5 * (9 / 5) + 32`
3. `scala> res0 / (9 / 5) - 32`
4. `scala> :load Hello.scala`
5. Paste complained about a missing class or object definition enclosing our greeting, I haven't seen the greeting
