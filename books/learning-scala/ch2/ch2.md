# Working with Data: Literals, Values, Variables, and Types

- A literal is data which appears directly in the source code
- A value is a immutable, typed storage unit
  - By convention are the default method for storing data
- A variable is a mutable, typed storage unit
- A type is the kind of data, a data classification

## Values
> `val <name>[: <type>] = <literal>`

- As values receive the data and don't ever change, it's type definition is optional once Scala's Compiler can infer it from the assigned value in a process called *type inference*. Note that this process happens only if there's no explicit type definition, an explicit type definition disables inference, so `val x: Int = "Hello"` would throw an error

## Variables
> `var <name>[: <type>] = <literal>`

- Prefer *values* over *variables*, as variables are dynamic, mutable and reassignable; while values are more predictable, stable, and easy to reason about
- Either, variable or value, must be initialized to be declared. Because of that, even mutable variables can have its type infered from the data type used to initialize it. However, the data type won't change on further assignments, if you use a data that doesn't match the type used at declaration, Scala will throw an error at you
- On reassignments, if the new type can be coersed to fit the previously designated type, Scala will convert it, e.g., assigning an `Int` to a variable declared as `Double`

## Naming

- `[]` are reserved for type parameterization
- Rules for naming:
  - A letter followed by zero or more letters and digits
  - A letter followed by zero or more letters and digits, then an underscore ( _ ), and then one or more of either letters and digits or operator characters.
  - One or more operator characters.
  - One or more of any character except a backquote, all enclosed in a pair of back-
quotes.
- By convention, value and variables should use camelCase to distinguish them from
types and classes which are named following PascalCase

## Types

- Scala has both numeric and nonnumeric types, which are the foundation for the others
- Unlike some languages, Scala doesn't have the concept of primitive types, its
`int` type is `Int`, so you don't have to be concerned about optimizations

### Numerical Data Types

- Can be converted based on the rank of the types, if the source fits in the destination.
Byte has the lowest rank (1 byte) and fits anywhere while Double (8 bytes) has the
biggest rank and can hold any numerical type below it
- Can be typed through *literal-characters* instead of type annotations
  - 5 for integer
  - 0x0f for a hexadecimal integer
  - 5l for a long
  - 5.0 or 5d for double
  - 5f for float

- Note: `Chars` can also be considered a *Numeric Data Type* because it can be converted to and from any number and is the basis for the `String` type

### Strings

- Scala strings are Java strings with some power-ups, both are enclosed by double quotes
- Unlike Java, `==` will check for value equality instead of object equality
- Multiline strings are created with """triple double quotes""" and are allow to write a backslash without a previous backslash, it seems that they don't handle escape sequences as usual
- String interpolated is done on strings prefixed by `s`, in which values are inserted through the use of `$` and if there's any operation enclosed by curly braces
  - s"Normal value $asdf"
  - s"Calculated value ${asdf * 5}"
- String interpolation can be done with `printf` notation by prefixing the string with `f`
  - f"Formated float $height%.2f"
  - f"Formated calculated value ${height * 2}%.5f"
- Regexes are base on `java.util.regex.Pattern` and can be created by converting normal strings to a Regex with `"Regex".r`
  - Values can be capture with regexes by using capturing groups with the syntax `val <Regex value>(<identifier for the captured value>) = <input string>` (This is possible because Regex values implement `unapply` and are [Extractor Objects](https://docs.scala-lang.org/tour/extractor-objects.html))
    ```scala
    val input = "Enjoying this apple 3.14159 times today"
    val pattern = """.* apple ([\d.]+) times .*""".r // taking advantage of triple quotes backslash
    val pattern(amountText) = input
    val amount = amountText.toDouble
    // > amount: Double = 3.14159
    ```

### An Overview of Scala Types

![The Scala type hierarchy](./type-hierarchy.png)

- Types that extend **AnyVal** are core values that represent data (can be allocated either on the heap or the stack)
- Types that extend **AnyRef** are reference values, usually used to group data to give it a meaning, and are always acessed through memory reference to an address on the heap
- **Nothing** is the bottom type for any type, it can't be instantiated, only used as a type to indicate things that have no value, like functions that don't return a value
- **Null** is the subtype of all **AnyRef** types and used to represent the *null* value

- Note: `&` and `&&` have different meanings from other languages, `&` isn't a binary and as expected. It's a *nonlazy* version of `&&`, meaning it evaluates both sides before returning a boolean value, instead of evaluating only the first and returning if there's no need to evaluate the second as the `lazy` version (`&&`) - The same applies to `|` and `||`

- Scala doesn't convert types to `Boolean` automatically, you either have to convert them explicitly by using `.toBoolean` or using a *function* that returns a `Boolean` or doing a comparison
- `Unit` is like `void` in other languages, it's used to denote the lack of data and can be "instantiated" with `()`
- Differently from Java, all values have classes and share some common methods

- Warning: Avoid `asInstanceOf` because the conversion isn't checked during compile time and will give you an error during runtime. Prefer `to<Type>` methods because they need to be declared on the class, thus having a somewhat "checked" conversion

### Tuples
> `(<value1>, <value2>[, <value3>...])

- Ordered and heterogenous container of two or more values, eg, `val info: (Int, String, Boolean) = (5, "Korben", true)`
- Differently from `Array`, values are **1** indexed and you access them with `info._<index>`
- Are useful for grouping discrete elements for handling

## Exercises

1. I expected everything to be Float once the input is a Float, however, I forgot that division is Int when both Int. Because of that, I was getting the wrong result
  ```scala
  object Types {
    def cToF(c: Float): Float = {
      // Remember that division is Int when both values are Int, you need to convert one to Float
      val ratio: Float = (c * (9/5f))
      val offset: Float = (ratio + 32)

      return offset
    }
  }
  ```
2. Just need to call `.toInt` on `offset`
3. Use `printf` notation with `f` interpolated strings, and escape the dollar sign with a double dollar sign
  ```scala
  val input: Float = 2.7255f
  val str = f"You owe $$$input%.2f"
  ```
4. Once `flag` is a Boolean we can negate it
  ```scala
  val flag: Boolean = false
  val result: Boolean = !flag
  ```
5. I was expecting the value to hold because I thought that the *String -> Double* conversion would ressemble the *Char -> Int* conversion and would use the character code to convert it to a Double. However, *String -> Double* conversion is based on the String content, if it's a number it'll convert, otherwise, it won't. `128.toChar.toString.toDouble.toInt` gives an error because the String isn't a number.
Instead of just using `.toDouble`, we would need to get the first String's char and convert it to Double, resulting in -> `val s = 128.toChar.toString; s(0).toDouble.toInt`
6. By using the regex extraction we remove the number and then split it on '-' and store on a tuple
  ```scala
  val str = "Frank,123 Main,925-555-1943,95122"
  val phoneRegEx = """.*?,.*,(.*?),.*""".r
  val phoneRegEx(phone) = str
  val phoneSections = phone.split("-").map(_.toInt)
  val phoneNumber = (phoneSections(0), phoneSections(1), phoneSections(2))
  ```
